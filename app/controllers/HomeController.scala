package controllers

import javax.inject._

import play.api._
import play.api.mvc._
import play.api.libs.ws._
import play.api.libs.json._

import scala.collection.parallel.mutable.ParHashMap
import scala.concurrent.duration._

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class HomeController @Inject()(config: Configuration, cc: ControllerComponents, ws: WSClient) extends AbstractController(cc) {

  val keysDB = new ParHashMap[String,JsValue].empty

  /**
   * Create an Action to render an HTML page.
   *
   * The configuration in the `routes` file means that this method
   * will be called when the application receives a `GET` request with
   * a path of `/`.
   */
  def index() = Action { implicit request: Request[AnyContent] =>
    Ok(Json.obj("about" -> "Argus Task"))
  }

  def getKey (key:String) = Action { implicit request: Request[AnyContent] =>
    val value = keysDB.get(key)
    value match {
      case Some(x) => Ok(x)
      case None => NotFound(Json.obj("message" -> JsString("Key " + key + " not found")))
    }
  }

  def postKey (key:String) = Action { implicit request: Request[AnyContent] =>
    try {
      val value = request.body.asJson
      keysDB.put(key, value.get)
      replicate(key,value.get)
      Ok(Json.obj("message" -> "Value stored OK"))
    }
    catch {
      case e:Exception => InternalServerError(Json.obj("message" -> JsString("Failed to store the value. Error is " + e) ))
    }
  }

  private def replicate(key:String, value: JsValue) = {
    try {
      val replicateNodeUrl = config.get[String]("replication.url")
      val request: WSRequest = ws.url(replicateNodeUrl + key)
      request.addHttpHeaders(("Content-Type", "application/json")).withRequestTimeout(5.second).post(value)
    }
    catch {
      case e:Exception => println("Replication failed on " + e)
    }
  }

  def replicateCallHandle(key:String) = Action { implicit request: Request[AnyContent] =>
    try {
      val value = request.body
      keysDB.put(key, value.asJson.get)
      Ok(Json.obj("message" -> "Value stored OK"))
    }
    catch {
      case e:Exception => InternalServerError(Json.obj("message" -> JsString("Failed to store the value. Error is " + e) ))
    }
  }
}
