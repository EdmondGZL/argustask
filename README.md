# Argus Backend Exercise #

This document will give a brief explanation on the code and the deployment process


### How do I get set up? ###

* Download the artifact from the Downloads section and unzip in both instances
* On every instance modify conf/application.conf to point to the other instance internal interface
* Run the service using: `nohup ./bin/argustask -Dplay.http.secret.key=dummy &` 
* Perform sanity check: `curl instance:address:8080/`, should return a simple JSON:
`{"about":"Argus Task"}`
* Invoke rest calls to any hostsas defined in the exercise as any instance is replicated to the other one
* Data isn't persisted to disk therfore restarting the service will delete all keys that have been stored on the instance

### About the code ###
 * The application was built on Scala Play framework (MVC model)
 * The route table is located on conf/routes
 * Replication is done simply by a Play rest client that invokes `POST /replicate/:key JSON` to the other node